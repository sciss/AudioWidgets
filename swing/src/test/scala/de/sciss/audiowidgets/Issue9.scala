package de.sciss.audiowidgets

import scala.swing.event.ValueChanged
import scala.swing.{BorderPanel, Frame, Label, MainFrame, SimpleSwingApplication, Swing}

object Issue9 extends SimpleSwingApplication {
  lazy val top: Frame = {
    val pf = new ParamField(123, Nil)
    pf.listenTo(pf)
    pf.reactions += {
      case ValueChanged(_) =>
        println(s"Change: value is ${pf.value}")
    }
    pf.tooltip = "Some parameter"

    new MainFrame {
      contents = new BorderPanel {
        add(pf, BorderPanel.Position.Center)
        add(new Label("A label"), BorderPanel.Position.South)
        border = Swing.EmptyBorder(10)
      }
    }
  }
}