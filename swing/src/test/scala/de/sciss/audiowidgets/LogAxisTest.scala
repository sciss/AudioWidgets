package de.sciss.audiowidgets

import scala.swing.{BorderPanel, BoxPanel, Frame, Label, MainFrame, Orientation, SimpleSwingApplication, Swing}

object LogAxisTest extends SimpleSwingApplication {
  lazy val top: Frame = new MainFrame {
    val xAxis: Axis = new Axis(Orientation.Horizontal) {
      minimum     =    20 // 44.1
      maximum     = 22050.0
//      format      = AxisFormat.Decimal
      logarithmic = true
    }
    xAxis.preferredSize = {
      val d = xAxis.preferredSize
      d.width = math.max(400, d.width)
      d
    }

    val yAxis: Axis = new Axis(Orientation.Vertical) {
      minimum     =     1.0
      maximum     = 10000.0
      logarithmic = true
    }
    yAxis.preferredSize = {
      val d = yAxis.preferredSize
      d.height = math.max(400, d.height)
      d
    }

    val top = new BoxPanel(Orientation.Horizontal)
    top.contents += Swing.HStrut(yAxis.preferredSize.width)
    top.contents += xAxis

    contents = new BorderPanel {
      add(top, BorderPanel.Position.North)
      add(yAxis, BorderPanel.Position.West)
      add(new Label("Logarithmic Axes"), BorderPanel.Position.Center)
    }
    pack().centerOnScreen()
  }
}
